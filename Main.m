clear all
clc
load('ecgConditioningExample.mat');
%% RAW SIGNALS
L=size(ecg);
time=(0:1:L(1)-1)/fs;
series0=ecg;

graphics(series0,fs);

N=[0]; %%Signal detection
for c=1:L(2)
    a=0;
    for s=1:L(1)
        if ecg(s,c)==0 | ecg(s,c)==NaN
            a=a+1;
        else
        end
    end
    if a>100 %Minimum number of invalid samples to consider the channel as wrongly connected
        N=[N,c];
    else
    end
end
N(1)=[];
for r=1:length(N)
    txt=['Channel ',num2str(N(r)),' is not connected properly'];
    disp(txt)
end

%% STEP 1: QRS DETECTOR + MEDIAN FILTER FOR MISSING VALUES
series1=zeros(L(1),L(2));
series1b=zeros(L(1),L(2));
meanPeak=zeros(L(2),1);

for n1=1:L(2) %% Spikes into NaNs
    [V I]=findpeaks(series0(:,n1),time,'MinPeakDistance',0.7);
    meanPeak(n1)=mean(V);
    for s=1:L(1)
        
        if series0(s,n1)>(meanPeak(n1)*2)%%Spike condition
            series1(s,n1)=NaN;
        else
            series1(s,n1)=series0(s,n1);
        end
    end
end

for m=1:L(2) %% Median to get value of spikes or other missing values (i.e NaNs)
series1b(:,m)=medfilt1(series1(:,m),5,'omitnan');
end

graphics(series1,fs);
%% Frequency analysis of one lead
F=fft(series0(:,2).');
F=abs(F);
F=F(1:ceil(end/2));
F=F/max(F);
f=(1:1:length(F))*((fs/2)/length(F));
plot(f,F);
xlabel('Frequency (Hz)');
%% STEP-2: Low-pass filter: high freq noise + powerline filtering (50Hz)
series2=zeros(L(1),L(2));
n=12;
wn=2*40/fs; %%(Covers 50Hz at least)
[numL denL]=butter(n,wn,'Low');
for n2=1:L(2)
    series2(:,n2)=filtfilt(numL,denL,series1b(:,n2));

end

graphics(series2,fs);

%% STEP-3 High-pass filter: low freq noise (Ex: baseline wander)
series3=zeros(L(1),L(2));
n=4;
wn=2*0.5/fs;
[numH denH]=butter(n,wn,'High');
for n2=1:L(2)
    series3(:,n2)=filtfilt(numH,denH,series2(:,n2));

end

graphics(series3,fs);
%% Get signal in mV if I know gain value (K)
% K=20000;
% series4=series3/K;
% graphics(series4,fs);
%% SIMPLIFYING ALTERNATIVE: BAND-PASS FILER 
series2=zeros(L(1),L(2));
% n=4;
% wn=2*[0.5 35]/fs;
% [numL denL]=butter(n,wn,'bandpass');
% for n2=1:L(2)
%     series2(:,n2)=filtfilt(numL,denL,series1b(:,n2));
% 
% end
% 
% graphics(series2,fs);