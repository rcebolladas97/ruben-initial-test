function graphics(signal,fs)

L=size(signal);
time=(0:1:L(1)-1)/fs;
figure;
tiledlayout(L(2),1)
 
for a=1:L(2)
    nexttile
    plot(time,signal(:,a));
    title0=['ECG lead - ', num2str(a)];
    title(title0)
    xlabel('time (s)')
 end

end

